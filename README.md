# Cluster Playground
### Как мне протестировать мой таск?
Для этого установить  [minikube](https://kubernetes.io/ru/docs/tasks/tools/install-minikube/), [kubectl](https://kubernetes.io/ru/docs/tasks/tools/install-kubectl/) и [helm](https://helm.sh/ru/docs/intro/install/). 
- Лучше использовать Virtualbox как бэкенд кластера, другие не тестировал 
- Рекомендую IDE OpenLens для удобного управления подами

Запускаем кластер
```
minikube start 
```
Он подумает минуту и запустится. На всякий случай проверим ноду
```
$ kubectl get nodes
NAME       STATUS   ROLES           AGE    VERSION
minikube   Ready    control-plane   126d   v1.26.3
```
Смотрим ip адрес кластера
```
$ minikube ip
192.168.59.103
```
Проще всего записать ip в hosts. Для примера я сделал такую запись. И для tcp и для https с типом балансировки prefix 
```
192.168.59.103 minikube.local balance-test.minikube.local
```

Теперь устанавливаем Ingress-контроллер 
```bash
helm upgrade --install ingress-nginx ingress-nginx \
--repo https://kubernetes.github.io/ingress-nginx \
--namespace ingress-nginx --create-namespace
```
Чтобы он работал нужно добавить ip кластера в конфиг сервиса, для этого
```
kubectl edit svc ingress-nginx-controller -n ingress-nginx
```
Отроется редактор, в раздел spec добавляем (вписываем ip кластера)
```yaml
spec:
  externalIPs:
  - 192.168.39.241 # minikube ip
```

Для тестирования балансировки нагрузки в кластере нужно скачать [вот этот](https://gitlab.com/BinaryBears/cluster-playground) репозиторий. 
### Для Web 
Устанавливаем Chart-web (установите sticky на false). 
```
helm upgrade --install nginx-hello ./Chart-web/ 
```
После этого переходим по http://balance-test.minikube.local и обновляем страницу несколько раз. Если ip и hostname меняется - то балансировка работает. А значит вы можете задеплоить чарт со своим таском, главное добавить адрес в hosts
![](https://i.imgur.com/FkquQlb.png)
#### Для TCP/SSH
Добавляем PortNamePrefix
```
helm upgrade --install ingress-nginx ingress-nginx   --repo https://kubernetes.github.io/ingress-nginx   --namespace ingress-nginx --create-namespace --reuse-values --set portNamePrefix=port
```
И открываете целевой порт как в примере выше:
```
helm upgrade --install ingress-nginx ingress-nginx   --repo https://kubernetes.github.io/ingress-nginx   --namespace ingress-nginx --create-namespace --reuse-values --set tcp.8888=default/tcp:8888
```
После этого обращаемся к таску по netcat
```
$ nc minikube.local 8888
Hostname: tcp-79c5d587b9-4pdsj
IP: 10.244.0.132
Random UUID: 3dd958b0-74c5-4eef-bfb9-ec81121d0d7a

$ nc minikube.local 8888
Hostname: tcp-79c5d587b9-kzk4v
IP: 10.244.0.131
Random UUID: a3b81b7e-646e-45ca-b61b-a9bcc86a3d43
```
TCP балансировка работает, можете проверять свой таск